## Description

[Description détaillée de la fonctionnalité et de son objectif]

## Tâches

- [ ] Tâche 1
- [ ] Tâche 2
- [ ] Tâche 3

## Critères d'acceptation

- [ ] Critère 1
- [ ] Critère 2
- [ ] Critère 3

## Documentation

[Liens vers toute documentation pertinente, ressources ou spécifications connexes]

## Tests

[Détails des tests qui doivent être effectués pour cette fonctionnalité]

## Dépendances

[Liste des autres tâches, problèmes ou demandes de fusion qui doivent être résolus avant de commencer cette fonctionnalité]

## Estimation

[Estimation approximative du temps nécessaire pour achever cette fonctionnalité]

## Personnes assignées

[Ajoutez les personnes assignées à cette tâche]

/cc @mention_personne