## Description

[Description détaillée de la demande de fusion]

## Problème résolu

[Référence au problème ou à l'issue associée]

## Changements proposés

[Détails sur les changements apportés par cette demande de fusion]

## Tests

[Détails des tests effectués pour vérifier les changements]

## Critères de validation

- [ ] Critère 1
- [ ] Critère 2
- [ ] Critère 3

## Documentation

[Liens vers toute documentation pertinente ou des ressources connexes]

## Dépendances

[Liste des autres tâches, problèmes ou demandes de fusion qui doivent être résolus avant de fusionner celle-ci]

## Personnes assignées

[Ajoutez les personnes assignées à cette demande de fusion]

/cc @mention_personne